/*---theoretical part---
1) Як можна оголосити змінну у Javascript?
Одним із способів оголосити про зміну JS - використовувати console.log(), 
для виведення повідомлення на консоль.Наприклад, якщо змінили зміну з ім'ям повідомлення ми пишимо:
console.log("The messege has been updated: +message");
Це виведеться на консолі, в якому буде:
"The message has been updated: [new value of message]".
2) У чому різниця між функцією prompt та функцією confirm?
Функція 'prompt' використовується для введення певного тексту.
Функція 'confirm' використовується для введення логічних значень (прохання користувача підтвердити або 
відхилити - вказати вибір користувача).
3) Що таке неявне перетворення типів? Наведіть один приклад.
Неявне перетворення типів - це автоматичне перетворення значення з одного типу даних в інший (без написання 
якогось явного коду для виконання перетворення).
Наприклад: у JS, коли додаємо число до рядка, то число автоматично становиться рядком, якщо два значення 
рядка - об'єднуються разом:

let num = 10;
let str = "30"
let result = num + str;(result "1030")*/


/*---practical part---*/
const admin = 'Victoriya';
console.log('Viktoriya');
const name = 'Victoriya';
alert('Victoriya');
alert('Victoriya');

let days = '8';
let seconds = '691200';
console.log(691200);
alert(8);
alert(691200);

const userName = prompt ("Write your name");
alert(userName);
console.log("Write your name");
const userAge = prompt ("How old are you?");
alert(userAge);
console.log("How old are you?");

const isUserSure = confirm ("Do you like our app?");
console.log(isUserSure);
if (isUserSure) {
    console.log("Yes");
} else {
    console.log("No");
}

